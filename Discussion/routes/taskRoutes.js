
// Contains all the endpoints for our application
// We separate the routes such that index.js only contains information on the server

const express = require("express");
const taskController = require("../controllers/taskController");

// Creates a Router instance that functions a middleware and routing system.
// Allow access to HTTp method middleware that makes it easier to create routes for our application.

const router = express.Router();

// Routes
// Theroutes are responsible for defining the URIs that out client accesses and the corresponding controller function that will be used whe a route is acceessed.
// They invoke the controller functions from the controller files
// All the business logic is done in the controller.

// Route to GET ALL THE TASK

router.get("/", (req, res) => {
	taskController.getAllTasks().then(resultFromController => res.send(resultFromController));
});

// Route for CREATING A NEW TASK

router.post("/", (req,res) => {

	taskController.createTask(req.body).then(resultFromController => res.send(resultFromController));
});

// Route for DELETING A TASK

router.delete("/:id", (req,res) => {

	console.log(req.params);

	taskController.deleteTask(req.params.id).then(resultFromController => res.send(resultFromController));
})

// Route for UPDATING A TASK

router.put("/:id", (req,res) => {

	taskController.updateTask(req.params.id, req.body).then(resultFromController => res.send(resultFromController));
})

// Route for GETTING SPECIFIC TASK

router.get("/:id", (req,res) => {

	taskController.getSpecificTask(req.params.id).then(resultFromController => res.send(resultFromController));
})

// Route for CHANGING THE STATUS OF A TASK

router.put("/:id/complete", (req,res) => {

	taskController.updateStatus(req.params.id, req.body).then(resultFromController => res.send(resultFromController));
})




// Use module.exports to export the router object to use in the index.js file

module.exports = router;