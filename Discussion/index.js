
// MODULES and PARAMETERIZED ROUTES

// Set up the dependancies

const express = require("express");
const mongoose = require("mongoose");

const taskRoute = require("./routes/taskRoutes")
// Set up the server

const app = express();
const port = 4000;

app.use(express.json());
app.use(express.urlencoded({extended: true}));

// Set up Database Connection
// Connecting to MongoDB Atlas

mongoose.set('strictQuery', false);
mongoose.connect("mongodb+srv://admin123:admin123@cluster0.yl6wic9.mongodb.net/s36?retryWrites=true&w=majority", {
	useNewUrlParser: true,
	useUnifiedTopology: true
});

let db = mongoose.connection;

db.on("err", console.error.bind(console, "Connection Error"));
db.once("open", () => console.log("We are connected to the cloud database"));

// Add the task route
// Allows all the task routes created in the "taskRoutes.js" file to use "/tasks" routes
app.use("/tasks", taskRoute);
// http://localhost:4000/tasks




app.listen(port, () => console.log(`Now listening to port ${port}`));
