
// Create the Schema, model and export the file

const mongoose = require("mongoose");
const taskSchema = new mongoose.Schema({

	name: String,
	status: {
		type: String,
		default: "pending"
	}
});

// The "Task" will become tasks as a collection name
module.exports = mongoose.model("Task", taskSchema);
// "module.exports" is a way for Node JS to treat this value as a "package that can be used in other files."