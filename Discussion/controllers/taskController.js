
// Controller contains the functions and business logic of our Express JS application
// Meaning all the operations it can do will be placed in this file

// Uses the require directive to allow accwes to the Task model which allows us to access the methods to perform CRUD operations
// Allows us to use the contents of the task.js file in the models folder

const Task = require("../models/task");

// Controller function for GETTING ALL THE TASKS
// Defines the functions to be used in the taskRoutes.js file and export these functions

module.exports.getAllTasks = () => {

	// The then method is used to wait for the Mongoose find method to finish before sending the result back to the route and eventually to the client/Postman
	return Task.find({}).then(result => {

		return result;
	});
}

module.exports.createTask = (requestBody) =>
{
	//  Creates a task object based on the Mongoose Model "task"
	let newTask = new Task({

		// Sets the "name" property with the value received from the client
		name: requestBody.name
	});

	// The first parameter will store the result that is return by the Mongoose save method
	// the second parameter will store the error object
	return newTask.save().then((task, error) => {

		if (error)
		{
			console.log(error)
			return false;
		}
		else
		{
			return task;
		}
	});
}

module.exports.deleteTask = (taskId) => {

	return Task.findByIdAndRemove(taskId).then((removedTask, err) => {

		if (err) {

			console.log(err)
			return false;
		}
		else
		{
			return removedTask;
		}
	});
}

module.exports.updateTask = (taskId, reqBody) => {

	return Task.findById(taskId).then((result, err) => {

		if (err) {
			console.log(error);
			return false;
		}
		else
		{
			result.name = reqBody.name;

			return result.save().then((updatedTask, saveErr) => {

				if (saveErr) {
					console.log(saveErr)
				}
				else
				{
					return updatedTask
				}
			})
		}
	})
}

module.exports.getSpecificTask = (taskId) => {

	return Task.findById(taskId).then((result, err) => {

		if (err) {
			console.log(error);
			return false;
		}
		else
		{
			return result;
		}
	})

}


module.exports.updateStatus = (taskId, reqBody) => {

	return Task.findById(taskId).then((result, err) => {

		if (err) {
			console.log(error);
			return false;
		}
		else
		{
			result.status = reqBody.status;

			return result.save().then((updatedTask, saveErr) => {

				if (saveErr) {
					console.log(saveErr)
				}
				else
				{
					return updatedTask
				}
			})
		}
	})
}